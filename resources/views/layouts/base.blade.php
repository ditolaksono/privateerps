<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="DashForge">
    <meta name="twitter:description" content="Responsive Bootstrap 4 Dashboard Template">
    <meta name="twitter:image" content="http://themepixels.me/dashforge/img/dashforge-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/dashforge">
    <meta property="og:title" content="DashForge">
    <meta property="og:description" content="Responsive Bootstrap 4 Dashboard Template">

    <meta property="og:image" content="http://themepixels.me/dashforge/img/dashforge-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/dashforge/img/dashforge-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Responsive Bootstrap 4 Dashboard Template">
    <meta name="author" content="ThemePixels">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="https://roomme.id/favicon.ico">

    <title>Personal ERP</title>

    <link href="{{ asset('lib/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/jqvmap/jqvmap.min.css') }}" rel="stylesheet">

    <!-- DashForge CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/dashforge.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/dashforge.dashboard.css') }}">
    <!-- DashForge CSS -->
    @yield('customCSS')
</head>
<body>
<aside class="aside aside-fixed">
    <div class="aside-header">
        <a href="{{ url('dashboard.index') }}" class="aside-logo">Space<span>ERP</span></a>
        <a href="" class="aside-menu-link">
            <i data-feather="menu"></i>
            <i data-feather="x"></i>
        </a>
    </div>
    <div class="aside-body">
        <div class="aside-loggedin">
            <div class="d-flex align-items-center justify-content-start">
                <a href="" class="avatar"><img src="https://via.placeholder.com/500" class="rounded-circle" alt=""></a>
                <div class="aside-alert-link">
                    <a href="" class="new" data-toggle="tooltip" title="You have 2 unread messages"><i data-feather="message-square"></i></a>
                    <a href="" class="new" data-toggle="tooltip" title="You have 4 new notifications"><i data-feather="bell"></i></a>
                    <a href="" data-toggle="tooltip" title="Sign out"><i data-feather="log-out"></i></a>
                </div>
            </div>
            <div class="aside-loggedin-user">
                <a href="#loggedinMenu" class="d-flex align-items-center justify-content-between mg-b-2" data-toggle="collapse">
                    <h6 class="tx-semibold mg-b-0">พิมพ์ชนก ลือวิเศษไพบูลย์</h6>
                    <i data-feather="chevron-down"></i>
                </a>
                <p class="tx-color-03 tx-12 mg-b-0">Administrator</p>
            </div>
            <div class="collapse" id="loggedinMenu">
                <ul class="nav nav-aside mg-b-0">
                    <li class="nav-item"><a href="" class="nav-link"><i data-feather="edit"></i> <span>Edit Profile</span></a></li>
                    <li class="nav-item"><a href="" class="nav-link"><i data-feather="user"></i> <span>View Profile</span></a></li>
                    <li class="nav-item"><a href="" class="nav-link"><i data-feather="settings"></i> <span>Account Settings</span></a></li>
                    <li class="nav-item"><a href="" class="nav-link"><i data-feather="help-circle"></i> <span>Help Center</span></a></li>
                    <li class="nav-item"><a href="" class="nav-link"><i data-feather="log-out"></i> <span>Sign Out</span></a></li>
                </ul>
            </div>
        </div><!-- aside-loggedin -->
        @include('layouts.navigations')
    </div>
</aside>

<div class="content ht-100v pd-0">
    <div class="content-header">
        <div class="content-search">
            <i data-feather="search"></i>
            <input type="search" class="form-control" placeholder="Search...">
        </div>
        <nav class="nav">
            <a href="" class="nav-link"><i data-feather="help-circle"></i></a>
            <a href="" class="nav-link"><i data-feather="grid"></i></a>
            <a href="" class="nav-link"><i data-feather="align-left"></i></a>
        </nav>
    </div><!-- content-header -->

    <div class="content-body">
        <div class="container pd-x-0">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb breadcrumb-style1 mg-b-10">
                            @yield('breadcrumb')
                        </ol>
                    </nav>
                    <h4 class="mg-b-0 tx-spacing--1">@yield('bc-title')</h4>
                </div>
            </div>
            @yield('body')
        </div><!-- container -->
    </div>
</div>
<div class="df-settings">
    <a id="dfSettingsShow" href="" class="df-settings-link"><i data-feather="settings"></i></a>
    <div class="df-settings-body">
        <div class="pd-y-20">
            <h3 class="tx-bold tx-spacing--2 tx-brand-02 mg-b-0">Space<span class="tx-normal tx-primary">ERP</span></h3>
            <span class="tx-sans tx-9 tx-uppercase tx-semibold tx-spacing-1 tx-color-03">Template Skin Customizer</span>
        </div>

        <div class="pd-y-20 bd-t">
            <label class="tx-sans tx-10 tx-uppercase tx-bold tx-spacing-1 tx-color-02 mg-b-15">Skin Mode</label>
            <div class="row row-xs">
                <div class="col-4">
                    <a href="" class="df-mode df-mode-default active" data-title="classic"></a>
                    <span class="df-skin-name">Classic</span>
                </div>
                <div class="col-4">
                    <a href="" class="df-mode df-mode-light" data-title="light">
                        <span class="bg-ui-01"></span>
                        <span class="bg-ui-02"></span>
                    </a>
                    <span class="df-skin-name">Light</span>
                </div>
                <div class="col-4">
                    <a href="" class="df-mode df-mode-cold" data-title="cool">
                        <span class="bg-primary op-1"></span>
                        <span class="bg-primary op-2"></span>
                    </a>
                    <span class="df-skin-name">Cool</span>
                </div>
                <div class="col-4 mg-t-15">
                    <a href="" class="df-mode df-mode-dark" data-title="dark">
                        <span class="bg-gray-700"></span>
                        <span class="bg-gray-900"></span>
                    </a>
                    <span class="df-skin-name tx-nowrap">Dark</span>
                </div>
            </div><!-- row -->
        </div>

        <div class="pd-y-20 bd-t">
            <label class="tx-sans tx-10 tx-uppercase tx-bold tx-spacing-1 tx-color-02 mg-b-15">Navigation Skin</label>
            <div class="row row-xs">
                <div class="col-4">
                    <a href="" class="df-skin df-skin-default active" data-title="default"></a>
                    <span class="df-skin-name">Default</span>
                </div>
                <div class="col-4">
                    <a href="" class="df-skin df-skin-deepblue" data-title="deepblue">
                        <span></span>
                        <span></span>
                    </a>
                    <span class="df-skin-name">Deep Blue</span>
                </div>
                <div class="col-4">
                    <a href="" class="df-skin df-skin-charcoal" data-title="charcoal">
                        <span></span>
                        <span></span>
                    </a>
                    <span class="df-skin-name">Charcoal</span>
                </div>
                <div class="col-4 mg-t-15">
                    <a href="" class="df-skin df-skin-gradient1" data-title="gradient1">
                        <span></span>
                        <span></span>
                    </a>
                    <span class="df-skin-name">Gradient 1</span>
                </div>
            </div><!-- row -->
        </div>

        <div class="pd-y-20 bd-t">
            <label class="tx-sans tx-10 tx-uppercase tx-bold tx-spacing-1 tx-color-02 mg-b-15">Font Family Base</label>
            <div class="row row-xs">
                <div class="col-7">
                    <a href="" id="setFontBase" class="btn btn-xs btn-outline-secondary active">IBM Plex Sans</a>
                </div><!-- col -->
                <div class="col-5">
                    <a href="" id="setFontRoboto" class="btn btn-xs btn-outline-secondary">Roboto</a>
                </div><!-- col -->
            </div><!-- row -->
        </div>
    </div><!-- df-settings-body -->
</div><!-- df-settings -->

<script src="{{ asset('lib/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('lib/feather-icons/feather.min.js') }}"></script>
<script src="{{ asset('lib/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
<script src="{{ asset('lib/jquery.flot/jquery.flot.js') }}"></script>
<script src="{{ asset('lib/jquery.flot/jquery.flot.stack.js') }}"></script>
<script src="{{ asset('lib/jquery.flot/jquery.flot.resize.js') }}"></script>
<script src="{{ asset('lib/chart.js/Chart.bundle.min.js') }}"></script>
<script src="{{ asset('lib/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('lib/jqvmap/maps/jquery.vmap.usa.js') }}"></script>

<script src="{{ asset('assets/js/dashforge.js') }}"></script>
<script src="{{ asset('assets/js/dashforge.aside.js') }}"></script>
<script src="{{ asset('assets/js/dashforge.sampledata.js') }}"></script>
<script src="{{ asset('assets/js/dashboard-one.js') }}"></script>
<script src="{{ asset('lib/jqueryui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
<script src="{{ asset('lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
<script src="{{ asset('lib/select2/js/select2.min.js') }}"></script>

<!-- append theme customizer -->
<script src="{{ asset('lib/js-cookie/js.cookie.js') }}"></script>
<script src="{{ asset('assets/js/dashforge.settings.js') }}"></script>
@yield('customJS')
</body>
</html>
