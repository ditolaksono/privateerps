@extends('layouts.base')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="#">Building</a></li>
    <li class="breadcrumb-item active" aria-current="page">Detail</li>
@endsection

@section('bc-title', $data->build_name)

@section('body')
    <div class="row row-xs">
        @foreach ($data->roomTypes as $roomType)
            <div class="col-sm-6 mb-3">
                <div class="card card-parent">
                    <div class="card-header bg-dark text-light">
                        <i data-feather="home"></i>
                        <span class="ml-2">
                            <strong>{{ $roomType->roomtype_name }}</strong>
                        </span>
                    </div>
                    <div class="card-body">
                        <div class="accordion">
                            @foreach($roomType->rooms as $room)
                                <h6 class="d-flex justify-content-between">
                                    <span>{{ $room->rooms_id }} - {{ $room->rooms_name }}</span>
                                    <span>
                                        @switch($room->rooms_status_id)
                                            @case(1)
                                            <span class="badge badge-info">Available</span>

                                            @break
                                            @case(2)
                                            <span class="badge badge-danger">Cleaning</span>
                                            @break
                                            @case(3)
                                            <span class="badge badge-danger">Suspended</span>
                                            @break
                                            @case(4)
                                            <span class="badge badge-danger">Ready</span>
                                            @break
                                            @case(5)
                                            <span class="badge badge-danger">Booked</span>
                                            @break
                                            @case(6)
                                            <span class="badge badge-danger">Closed</span>
                                            @break
                                            @case(7)
                                            <span class="badge badge-danger">Not Ready</span>
                                            @break
                                        @endswitch
                                        @if ($room->rooms_del_status === '0')
                                            <span class="badge badge-success">ENABLED</span>
                                        @else
                                            <span class="badge badge-warning">DISABLED</span>
                                        @endif
                                    </span>
                                </h6>
                                <div>
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Book no</th>
                                            <th>Check-in</th>
                                            <th>Check-out</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($room->booking as $booking)
                                            <tr
                                        id="book-table-{{ $booking->book_id }}" data-toggle="modal"
                                        row-room-data="{{ json_encode($room) }}"
                                        row-book-data="{{ $booking }}"
                                        row-customer-data="{{ json_encode($booking->customer) }}"
                                                @if (!($booking->book_check_out > now()->toDateString()))
                                                class="bg-info"
                                                @endif>
                                                <td>{{ substr($booking->book_no, 0, 6) }}</td>
                                                <td>{{ date('d M Y', strtotime($booking->book_check_in)) }}</td>
                                                <td>{{ date('d M Y', strtotime($booking->book_check_out)) }}</td>
                                                <td>
                                                    @switch($booking->book_status_id)
                                                        @case(1)
                                                        Order Payment Confirmed
                                                        @break

                                                        @case(2)
                                                        Order Filed
                                                        @break

                                                        @case(3)
                                                        Order Waiting for Payment
                                                        @break

                                                        @case(4)
                                                        <span class="text-danger">
                                                            <strong>
                                                                Order Cancelled
                                                            </strong>
                                                        </span>
                                                        @break

                                                        @case(5)
                                                        Under Maintenance
                                                        @break

                                                        @case(6)
                                                        Order Payment Deposit Confirmed
                                                        @break

                                                        @case(7)
                                                        In
                                                        @break

                                                        @case(8)
                                                        Out
                                                        @break

                                                        @case(9)
                                                        Extend
                                                        @break

                                                        @case(10)
                                                        Warning
                                                        @break

                                                        @case(11)
                                                        Order Payment Full
                                                        @break
                                                    @endswitch
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content tx-14">
            <div class="modal-header">
              <h6 class="modal-title" id="room_title"></h6>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Customer name : <strong id="modal-customer-name"></strong></p>
              <p>Check in : <strong id="modal-check-in"></strong></p>
              <p>Check out : <strong id="modal-check-out"></strong></p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary tx-13" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary tx-13">Save changes</button>
            </div>
          </div>
        </div>
      </div>
@endsection

@section('customJS')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.26.0/moment.min.js"></script>
    <script>
        $(function () {
            $('.accordion').accordion({
                heightStyle: 'content',
                collapsible: true
            });

            $('[id*=book-table]').on('click', function () {
                var bookingData = $(this).attr('row-book-data');
                var roomData = $(this).attr('row-room-data');
                var customerData = $(this).attr('row-customer-data');

                bookingData = JSON.parse(bookingData);
                roomData = JSON.parse(roomData);
                customerData = JSON.parse(customerData);

                var payLog = JSON.parse(bookingData.book_pay_log);

                var modal = $('#modal2');
                modal.modal();

                modal.find('#room_title').html(roomData.rooms_id + " " + roomData.rooms_name);
                modal.find('#modal-customer-name').html(customerData.cust_name);
                

                var checkIn = bookingData.book_check_in, checkOut = bookingData.book_check_out, cyclePay = 0;

                var results = [];

                if (payLog) {
                    if (payLog.deposit.status === 'paid') {

                        for(var i = 0; i < payLog.paymentCycle.length; i++) {
                            for(key in payLog.paymentCycle[i]) {
                                if(payLog.paymentCycle[i][key] === 'paid') {
                                    results.push(payLog.paymentCycle[i]);
                                }
                            }
                        }

                        checkIn = payLog.paymentCycle[0].period.start;

                        if ((results.length === 0)) {
                            checkOut = payLog.paymentCycle[0].period.end;
                        } else {
                            checkOut = payLog.paymentCycle[results.length - 1].period.end;
                        }
                    }
                }

                modal.find('#modal-check-in').html(moment(checkIn).format('D MMM Y'));
                modal.find('#modal-check-out').html(moment(checkOut).format('D MMM Y'));

                
            });
        });
    </script>
@endsection

@section('customCSS')
    <style>
        .ui-accordion-content {
            padding: 0 10px 20px 10px
        }
    </style>
@endsection
