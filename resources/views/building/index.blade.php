@extends('layouts.base')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
    <li class="breadcrumb-item active" aria-current="page">Building</li>
@endsection

@section('bc-title', 'List building')

@section('body')
    <div class="row row-xs">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <form method="get">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="check_in">Check in</label>
                                    <input type="text" class="form-control" name="check_in" id="check_in">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="check_in">Check out</label>
                                    <input type="text" class="form-control" name="check_out" id="check_out">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Build name</th>
                            <th>Room available</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($buildings as $building)
                            <tr row-data="{{ $building->build_id }}">
                                <td>
                                    {{ $building->build_id }}
                                </td>
                                <td>
                                    <a href="{{ route('dashboard.building.detail', ["id" => $building->build_id]) }}">
                                        {{ $building->build_name }}
                                    </a>
                                </td>
                                <td>
                                    {{ isset($building->roomTypes[0]) ? $building->roomTypes[0]->rooms()->count() : 0}}
                                </td>
                                <td>
                                    <a href="#" class="text-danger">
                                        <i data-feather="trash-2" width="16"></i>
                                    </a>
                                    <a href="#" class="text-info">
                                        <i data-feather="file-text" width="16"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('customJS')
    <script>
        $(function () {
            $('#check_in, #check_out').datepicker();

            $('.table').DataTable({
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });
        })
    </script>
@endsection
