<?php

namespace App\Http\Controllers\Building;

use App\Http\Controllers\Controller;
use App\Models\Building;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ListAllBuildingAction extends Controller
{
    public function __invoke()
    {
        $buildings = Building::all();

        return view('building.index', [
            'buildings' => $buildings
        ]);
    }
}
