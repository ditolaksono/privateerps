<?php

namespace App\Http\Controllers\Building;

use App\Http\Controllers\Controller;
use App\Models\Building;
use App\Models\RoomType;
use Illuminate\Http\Request;

class GetOneBuildingAction extends Controller
{
    public function __invoke($id)
    {
        $data = Building::find($id);

        return view('building.detail', [
            'data' => $data,
        ]);
    }
}
