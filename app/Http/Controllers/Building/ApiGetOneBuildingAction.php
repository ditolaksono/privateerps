<?php

namespace App\Http\Controllers\Building;

use App\Http\Controllers\Controller;
use App\Models\Building;
use Illuminate\Http\Request;

class ApiGetOneBuildingAction extends Controller
{
    public function __invoke($id)
    {
        $data = Building::findOrFail($id);

        return response()->json($data);
    }
}
