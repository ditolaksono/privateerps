<?php

namespace App\Http\Controllers\Support;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ShowSettingsPageAction extends Controller
{
    public function __invoke()
    {
        return view('settings');
    }
}
