<?php


namespace App\Http\Services\Room;


class RoomService
{
    public function getLockedRooms($roomType, $checkIn, $checkOut)
    {
        $bookings = DB::table('booking')
            ->select([
                'book_id',
                'book_room_id',
                'book_pay_log'
            ])
            ->where('book_pay_log->deposit->status', 'paid')
            ->where('book_room_type_id', $roomType)
            ->whereIn('book_status_id', [1, 6, 7, 11])
            ->get();

        $roomsLocked = [];

        $bookings->each(function ($book) use ($checkIn, $checkOut, &$roomsLocked) {
            $book->logs = collect(json_decode($book->book_pay_log));

            $paidInvoicesPeriod = collect($book->logs['paymentCycle'])
                ->where('status', 'paid');

            $firstPaymentCycle = collect($book->logs['paymentCycle'])
                ->first();

            $startDate = Carbon::parse($firstPaymentCycle->period->start);
            $endDate = Carbon::parse($firstPaymentCycle->period->end);

            if (isset($paidInvoicesPeriod[0])) {

                # Getting value while booking have at least 1 paymentCycle paid!
                $startDate = Carbon::parse($paidInvoicesPeriod[0]->period->start);
                $endDate = Carbon::parse(
                    $paidInvoicesPeriod[$paidInvoicesPeriod->count() - 1]->period->end
                );
            }

            $desiredCheckIn = Carbon::parse($checkIn);
            $desiredCheckOut = Carbon::parse($checkOut);

            if (
                $desiredCheckIn->between($startDate, $endDate) ||
                $desiredCheckOut->between($startDate, $endDate)
            ) {
                array_push($roomsLocked, $book->book_room_id);
            } else if (
                $startDate->between($desiredCheckIn, $desiredCheckOut) ||
                $endDate->between($desiredCheckIn, $desiredCheckOut)
            ) {
                array_push($roomsLocked, $book->book_room_id);
            }

            /**
             * echo "$book->book_room_id locked from " . $startDate->toDateString() . " until " .
             * $endDate->toDateString() . ", due to " .
             * ($paidInvoicesPeriod->count() !== 0 ? $paidInvoicesPeriod->count() . " cycle" : 'deposit') .
             * " paid\n";
             */
        });

        return $roomsLocked;
    }

    public function getRoomAvailable($roomType, $checkIn, $checkOut)
    {
        return DB::table('rooms')
            ->where('rooms_type_id', $roomType)
            ->whereNotIn('rooms_id', $this->getLockedRooms($roomType, $checkIn, $checkOut))
            ->where('rooms_status_id', 1)
            ->get();
    }

    public function countRoomListByBuilding($buildId, $checkIn, $checkOut)
    {
        $roomTypes = DB::table('room_type')
            ->where('roomtype_build_id', $buildId)
            ->get();

        $count = 0;

        $roomTypes->each(function ($roomType) use (&$count, $checkIn, $checkOut) {
            $count += $this->getRoomAvailable($roomType->roomtype_id, $checkIn, $checkOut)->count();
        });

        return $count;
    }
}
