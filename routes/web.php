<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'dashboard',
], function () {
    Route::get('/', 'Dashboard\ShowDashboardAction')
        ->name('dashboard.index');
    Route::get('/setting', 'Support\ShowSettingsPageAction')
        ->name('dashboard.setting');

    Route::group([
        'prefix'    => 'building',
        'namespace' => 'Building',
    ], function () {
        Route::get('/', 'ListAllBuildingAction')->name('dashboard.building.index');
        Route::get('/{id}', 'GetOneBuildingAction')
            ->where('id', '[0-9]+')
            ->name('dashboard.building.detail');
    });
});
